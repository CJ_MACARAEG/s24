// Cube

const getCube = 2**3

message = `the cube 2 is ${getCube}`

console.log(message);

//Cube End

//Array in Address
const fullAddress = ["258 Washington Ave NW", "California", 90011]

const [houseStreet, State, postalCode] = fullAddress
console.log(`I live at ${houseStreet}, ${State} ${postalCode}`)

// Animal

const animal = {
	animalName: "Lolong",
	animalType: "saltwater crocodile",
	animalWeight: 1075,
	animalHeight: 20
}

console.log(`${animal.animalName} was a ${animal.animalType}. He weighed at ${animal.animalWeight} kgs with a measurement of ${animal.animalHeight} ft 3 in. `);

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(`${number}`)
});

let value = 0;

const reduceNumbers = numbers.reduce((x, y) => x + y, value);

console.log(reduceNumbers);



